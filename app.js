/* eslint-disable no-console */
const express = require('express');
const db = require('./queries');
const funcs = require('./funcoes');

var app = express();
var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

app.use(express.json());
app.use(express.urlencoded({
  extended: true,
}));

app.get('/', funcs.raiz);
app.post('/fileupload', funcs.fileDownload);
app.post('/insereUsuario', db.insereUsuario);
//insereQuestao,
//insereRegistro,
app.post('/loginAndPasswordMatch', db.loginAndPasswordMatch);
app.post('/getUserIdbyLogin', db.getUserIdbyLogin);
app.get('/updateTermsFromUser', db.updateTermsFromUser);
app.get('/testEndpoint', funcs.test);

app.listen(3000, function () {
  console.log('App escutando na porta 3000!');
});
















function normalizePort(val) {
  var port = parseInt(val, 10);
  if (isNaN(port)) {
    // named pipe
    return val;
  }
  if (port >= 0) {
    // port number
    return port;
  }
  return false;
}


//app.use(bodyParser.json({limit: '10mb', extended: true}))
//app.use(bodyParser.urlencoded({limit: '10mb', extended: true, parameterLimit: 1000000}))






//  let arquivo =  JSON.stringify(req.body, null, 0);

//    fs.writeFile("/home/marc/imgtest.png", arquivo, 'base64', function(err) {
//        if(err){
//            return console.log(err);
//        }
//    console.log("arquivo salvo");
//    });

// print to console
// console.log(req.body);

// just call res.end(), or show as string on web

//    console.log(JSON.stringify(req.body, null, 0));

//	res.send(JSON.stringify(req.body, null, 4));
//});







/* eslint-disable no-console */

// Talvez no futuro trocar o express por Fastify

