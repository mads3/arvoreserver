/* eslint-disable no-console */
var AdmZip = require('adm-zip');

//app.get('/', function 
const raiz = (req, res) => {
    res.writeHead(200, {
        'Content-Type': 'text/html'
    });
    res.write('<form action="fileupload" method="post" enctype="multipart/form-data">');
    res.write('<input type="file" name="filetoupload"><br>');
    res.write('<input type="submit">');
    res.write('</form>');
    return res.end()
    //  res.send('Hello World!');
    //  console.log('get world!');
};

//app.post('/fileupload', function
const fileDownload = (req, res) => {
    var formidable = require('formidable');
    var fs = require('fs');
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        console.log("fields:"); console.log(fields);
        console.log("files:"); console.log(files);
        console.log("\n\n###################\n\n");
        var oldpath = files.file.path;
        console.log("O OLDPATH E: " + oldpath);
        var d = new Date();
        var newpath = './temp/' + d.getTime() + files.file.name;
        console.log("O NEWPATH E: " + newpath);
        fs.rename(oldpath, newpath, function (err) {
            if (err) throw err;
            res.write('fs.rename error');
            res.end();
        });
        descompacta(newpath, newpath);
        //adiciona(newpath)
    });


};

const descompacta = (arquivoPath, destino) => {
    var zip = new AdmZip(arquivoPath);
    var zipEntries = zip.getEntries();
    var img_Inteira, img_Copa, img_Folha;
    var jsonObj;
    zipEntries.forEach(function(zipEntrie) {
        if(zipEntrie.name.includes('Copa')){
            img_Copa = zipEntrie.getData();
            console.log("Entrou na copa");
        }
        if(zipEntrie.name.includes('Inteira')) {
            img_Inteira = zipEntrie.getData();
            console.log("Entrou na inteira");
        }
        if(zipEntrie.name.includes('Folha')) {
            img_Folha = zipEntrie.getData();
            console.log("Entrou na folha");
        }
        if(zipEntrie.name.includes('infos')) {
            jsonObj = JSON.parse(zipEntrie.getData());
            console.log("Entrou na infos");

        }
//            db.insereImagem(zipEntrie.getData, "img_um");
    });
    
    require('./queries').insereTudo(img_Inteira, img_Copa, img_Folha, jsonObj["EnderecoArvore"], jsonObj["EspecieArvore"], jsonObj["Fruto"], jsonObj["Gravidade"], jsonObj["InformacoesAdicionais"], 1, "temquearrumar", jsonObj["CopaLat"], jsonObj["CopaLon"]);
}
 

function passwordHash() {
    console.log('Do some password hashing');
}

function test(req, res) {
    console.log("comecou");
    setTimeout(function() {
                console.log("terminou");
    }, 8000);
    res.status(200).json('sucesso');
}



module.exports = {
    raiz,
    fileDownload,
    descompacta,
    passwordHash,
    test,
}