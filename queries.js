/* eslint-disable no-console */
const Pool = require('pg').Pool
const pool = new Pool({
        user: 'dbmaster',
        host: '200.236.3.108',
        database: 'arvoredb',
        password: 'm@rcelo2019',
        port: 5432,
})

const insereUsuario = (request, response) => {
        const login = request.body.login;
        const password = request.body.password;
        const nome = request.body.nome;
        const phone_number = request.body.phone;
        const termo = true;
        pool.query('INSERT INTO usuario (login, nome, password, phone_number, termo) VALUES ($1, $2, $3, $4, $5)', [login, password, nome, phone_number, termo], (error, results) => {
                if (error) {
                        console.log("ERROR: " + error);
                        throw error;
                }
                console.log("ADICIONOU USER" + login);
                response.status(200).json('sucesso');
        });
}

const insereQuestao = (request, response) => {
        const rua = request.params.rua;
        const especie = request.params.especie;
        const fruto = request.params.fruto;
        const gravidade = request.params.gravidade;
        const infos = request.params.infos;
        pool.query('INSERT INTO questoes (rua, especie, fruto, gravidade, infos) VALUES($1, $2, $3, $4, $5);', [rua, especie, fruto, gravidade, infos], (error, results) => {
                if (error) {
                        console.log("ERROR_QUESTAO: " + error);
                        throw error;
                }
                console.log("ADICIONOU QUESTAO" + infos);
                response.status(200).json('sucesso');
        });
}

const insereRegistro = (request, response) => {
        const id_tipo = request.params.id_tipo;
        const id_questoes = request.params.id_questoes;
        const latitude = request.params.latitude;
        const longitude = request.params.longitude;
        const id_img_um = request.params.id_img_um;
        const id_img_dois = request.params.id_img_dois;
        const id_img_tres = request.params.id_img_tres;
        pool.query('INSERT INTO registro (id_tipo, id_questoes, latitude, longitude, id_img_um, id_img_dois, id_img_tres) VALUES($1, $2, $3, $4, $5, $6, $7);', [id_tipo, id_questoes, latitude, longitude, id_img_um, id_img_dois, id_img_tres], (error, results) => {
                if (error) {
                        console.log("ERROR_REGISTRO: " + error);
                        throw error;
                }
                console.log("ADICIONOU REGISTRO" + results);
                response.status(200).json('sucesso');
        });
} 

const loginAndPasswordMatch = (req, res) => {
        console.log(req.body);
        const login = req.body.login;
        const password = req.body.password;
        console.log('login: ' + login + ' password: ' + password);
        pool.query("SELECT * FROM usuario WHERE login = $1 AND password = $2", [login, password], (error, results) => {
                if (error) {
                        console.log("loginAndPasswordMatch error");
                        throw error;
                }
                console.log("loginAndPasswordMatch sucesso");
                res.status(200).json(results.rowCount);

        });


}

const getUserIdbyLogin = (req, res) => {
        console.log(req.body);
        const login = req.body.user_id;
        pool.query("SELECT id FROM usuario WHERE login = $1", [login], (error, results) => {
                if (error) {
                        throw error;
                }
                res.status(200).results.row[0];
        });
}



const updateTermsFromUser = (req, res) => {
        console.log(req.body);
        const login_usuario = req.body.login_usuario;
        pool.query("UPDATE usuario SET termo = true WHERE login = $1", [login_usuario], (error, results) => {
                if (error) {
                        throw error;
                }
                res.status(200).json(results.rowCount);   // Sucesso retorna 1, Falha 0

        });
}
    
const termosAceitos = (req, res) => {
	const login = req.body.login;
	pool.query("SELECT termo FROM usuario WHERE login = $1", [login], (error, results) => {
		if (error) {
			throw error;
		}
		res.status(200).json(results);
	});
}

const insereImagem = (imageStream, tabela) => {
        pool.query("INSERT INTO " + tabela + " (imagem) VALUES($1) RETURNING id;", [imageStream], (error, results) => {
                if (error) {
                        throw error;
                }
                console.log("INSERE IMAGEM: ");
                console.log(results.rows[0].id);
        })
}

function insereTudo(imgInteira, imgCopa, imgFolha, rua, especie, fruto, gravidade, infos, tipo_registro, tipo_de_registro, latitude, longitude ) {
        console.log("chegou aqui.");
        console.log(rua);
        pool.query("                                                            \
        with ins_img_um as (                                                    \
                insert into img_um(imagem)                                      \
                values($1)                                                  \
                RETURNING id as id_um                                           \
              ),  ins_img_dois as (                                             \
                insert into img_dois(imagem)                                    \
                values($2)                                                  \
                RETURNING id as id_dois                                         \
              ),  ins_img_tres as (                                             \
                insert into img_tres(imagem)                                    \
                values($3)                                                  \
                RETURNING id as id_tres                                         \
              ), ins_questoes as (                                              \
                insert into questoes(rua, especie, fruto, gravidade, infos)     \
                values($4,$5,$6,$7,$8)                                       \
                RETURNING id as id_questoes                                     \
              ), ins_tipo as (                                                  \
                insert into tipo(tipo_registro, tipo_de_registro)               \
                values($9,$10)                                                   \
                RETURNING id as id_tipo                                         \
              )                                                                 \
              INSERT INTO registro(id_tipo, id_questoes, latitude, longitude, id_img_um, id_img_dois, id_img_tres)              \
              VALUES ((select id_tipo from ins_tipo), (select id_questoes from ins_questoes), $11, $12,                         \
               (select id_um from ins_img_um), (select id_dois from ins_img_dois), (select id_tres from ins_img_tres));   "
               ,[imgInteira, imgCopa, imgFolha, rua, especie, fruto, gravidade, infos, tipo_registro, tipo_de_registro, latitude, longitude], (error, results) => {
                       if(error) {
                               throw error;
                       }
               console.log("RESULTADO DA MEGA INSERCAO EH: " + results);
                })
}
       
const insereTudo2 = (varias, coisas) => {
        pool.query("\
        BEGIN TRANSACTION;\
        INSERT INTO ...\
        VALUES ...\
        INSERT INTO ...\
        VALUES ...\
        END TRANSACTION;"), [variascoisas], (error, results) => {
                if(error) {
                        throw error;
                }
                console.log("TRANSACTION RODOU BEM");
        }
}
 


module.exports = {
        insereUsuario,
        insereQuestao,
        insereRegistro,
        loginAndPasswordMatch,
        getUserIdbyLogin,
        updateTermsFromUser,
        termosAceitos,
        insereImagem,
        insereTudo


}

